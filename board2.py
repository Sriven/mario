'''Prints the board'''

from colorama import Fore


class Boardclass:
    '''class for board'''

    def __init__(self, rows, columns, board_map):
        '''Initializing the entire board'''

        self.rows = rows
        self.columns = columns
        self.x_pos = 0
        self.y_pos = 0

        self.list2 = [[0 for x_t in range(self.columns)]
                      for y_loop in range(self.rows)]

        self.mat = [[0 for x_t in range(150)] for y_l in range(self.rows)]

        for j in range(self.rows):

            if j > (self.rows - 6):
                for i in range(columns):
                    self.list2[j][i] = 'X'

            else:
                for i in range(columns):
                    if i == 500:
                        self.list2[j][i] = 'X'
                    else:
                        self.list2[j][i] = ' '

        for k in range(6):

            x_in, y_in = board_map.cloudsloc(k)

            cloud = board_map.returncloud()

            for i in range(5):
                for j in range(14):
                    self.list2[x_in + i][y_in + j] = Fore.WHITE + \
                        cloud[i][j] + Fore.RESET

        for k in range(6):

            x_in, y_in = board_map.pipeloc(k)

            pipe = board_map.returnpipe()

            for i in range(9):
                for j in range(14):
                    self.list2[x_in + i][y_in + j] = pipe[i][j]

        for k in range(5):

            x_in, y_in = board_map.ledgeloc(k)

            ledge = board_map.returnledge()

            for i in range(3):
                for j in range(11):
                    self.list2[x_in + i][y_in + j] = ledge[i][j]

    def move_screen(self, inp, mario):
        '''To move screen'''

        if inp == 'a':

            if self.x_pos >= 2 and self.check_horizontal_collision(mario, 'a') is False:
                self.x_pos -= 2

        if inp == 'd':

            if self.x_pos <= 1448 and self.check_horizontal_collision(mario, 'd') is False:
                self.x_pos += 2

    def print_screen(self, mario, board_map, enemies, bosses):
        '''To Print screen'''

        self.mat = [[0 for x_t in range(150)] for y_l in range(self.rows)]

        for i in range(self.rows):
            for j in range(self.x_pos, self.x_pos + 150):
                self.mat[i][j - self.x_pos] = self.list2[i][j]

        for i in range(3):
            for j in range(4):
                self.mat[mario.y_pos + i - 2][73 + j] = Fore.GREEN + \
                    board_map.marioshape[i][j] + Fore.RESET

        for enemy in enemies:
            if enemy.x_pos >= (self.x_pos + 10) and enemy.x_pos <= (self.x_pos + 140):
                for i in range(3):
                    for j in range(4):
                        self.mat[enemy.y_pos + i][enemy.x_pos - self.x_pos +
                                                  j] = Fore.RED + board_map.enemyshape[i][j] + Fore.RESET

        for boss in bosses:
            if boss.x_pos >= (self.x_pos + 10) and boss.x_pos <= (self.x_pos + 135):
                for i in range(11):
                    for j in range(9):
                        self.mat[boss.y_pos + i][boss.x_pos - self.x_pos +
                                                 j] = Fore.MAGENTA + board_map.bossshape[i][j] + Fore.RESET

        for i in range(self.rows):
            for j in range(150):

                if i > 26:
                    print(Fore.GREEN + self.mat[i][j] + Fore.RESET, end='')

                elif self.mat[i][j] == 'X':
                    print(Fore.RED + self.mat[i][j] + Fore.RESET, end='')

                else:
                    print(self.mat[i][j], end='')

            print()

        return self.mat

    def check_horizontal_collision(self, mario, inp):
        '''To check collision'''

        if inp == 'd':

            return self.mat[mario.y_pos][77] == 'X' or self.mat[mario.y_pos][78] == 'X'

        if inp == 'a':

            return self.mat[mario.y_pos][72] == 'X' or self.mat[mario.y_pos][71] == 'X'

    def return_x(self):
        '''just return xpos'''
        return self.x_pos
