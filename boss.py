'''Boss Class'''
from enemies import Enemies

class Boss(Enemies):
    """Boss"""

    def __init__(self, x_in, y_in):

        super().__init__(x_in, y_in)
        self.vel = 2

    def move(self):
        '''Boss moves faster'''

        if self.dist >= 30:

            self.vel = -2

        if self.dist <= -30:

            self.vel = 2

        self.x_pos += self.vel
        self.dist += self.vel

    def kill_me(self, mario, x_in):
        '''To kill BOSS'''

        if mario.y_pos < self.y_pos:
            if x_in + 73 - self.x_pos > 0:
                if x_in + 73 - self.x_pos < 10:
                    return True

        return False
