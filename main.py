'''Come at me, Bro'''

import os
import sys
from board2 import Boardclass
from character import Mario
from map import Map
from enemies import Enemies
from boss import Boss
from input import Get, input_to

# Initialization of objects

MAP = Map()
BOARD = Boardclass(32, 800, MAP)
MARIO = Mario(75, 26)
CH = Get()
Y, X = MAP.bossloc()
BOSSES = []
BOSSES.append(Boss(X, Y))
BOSS_KILLED = False
ENEMIES = []
SCORE = 0
LIVES = 3
DEL_ENEMY = []

for i in range(5):
    Y, X = MAP.enemyloc(i)
    ENEMIES.append(Enemies(X, Y))

MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

# Main Game LOOP

while True:

    INPUT_X = input_to(CH)
    os.system('clear')

# Checking the status of ENEMIES and boss

    for i in range(len(ENEMIES)):
        ENEMIES[i].move()

        if ENEMIES[i].kill_me(MAT, BOARD.return_x()) is True:
            DEL_ENEMY.append(i)
            SCORE += 10

        if ENEMIES[i].die_mario(BOARD.return_x(), MARIO) is True:
            print("GAME OVER")
            print("Score =", SCORE)
            print("Mario never found his princess :(")
            os.system('aplay -q gameover.wav&')
            sys.exit()

    for i in DEL_ENEMY:
        del ENEMIES[i]
        DEL_ENEMY = []

    for boss in BOSSES:

        boss.move()

        if boss.die_mario(BOARD.return_x(), MARIO, 11) is True:
            print("GAME OVER")
            print("Score =", SCORE)
            print("Mario never found his princess :(")
            os.system('aplay -q gameover.wav&')
            sys.exit()

        if boss.kill_me(MARIO, BOARD.return_x()) is True:
            SCORE += 500
            BOSS_KILLED = True

    if BOSS_KILLED is True:
        try:
            del BOSSES[0]
        except:
            pass

# Printing Screen

    MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

    if INPUT_X == 'q':
        os.system('aplay -q gameover.wav&')
        sys.exit()

    MARIO.check_jump(MAT)

# Checking Input

    if INPUT_X == 'a':
        BOARD.move_screen('a', MARIO)

    if INPUT_X == 'd':
        BOARD.move_screen('d', MARIO)

    if INPUT_X == 'w':
        MARIO.move('w')

    if BOARD.x_pos == 422:
        print("You WON!")
        print("Please Play Again")
        print("Final Score =", SCORE)

        os.system('aplay -q stage.wav&')
        sys.exit()

    print("SCORE =", SCORE)
    print("X = ", BOARD.x_pos)
    print("Y = ", MARIO.y_pos)
