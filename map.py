'''Map Config File'''

class Map():
    '''map config class'''

    def __init__(self):
        '''Config file for map'''

        self._clouds = [[' ', ' ', '_', '_', '_', '_', '_',
                         '_', '_', '_', '_', '_', ' ', ' '],
                        [' ', '/', '-', '-', '-', '-', '-',
                         '-', '-', '-', '-', '-', '\\', ' '],
                        ['/', '-', '-', '-', '-', '-', '-',
                         '-', '-', '-', '-', '-', '-', '|'],
                        ['\\', '-', '-', '-', '-', '-', '-',
                         '-', '-', '-', '-', '-', '/', ' '],
                        [' ', '\\', '_', '_', '_', '_', '_',
                         '_', '_', '_', '/', ' ', ' ', ' ']]

        self._pipe = [['X', 'X', 'X', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', 'X', 'X', 'X'],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' '],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' '],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' '],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' '],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' '],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' '],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' '],
                      [' ', ' ', ' ', 'X', 'X', 'X', 'X',
                       'X', 'X', 'X', 'X', ' ', ' ', ' ']]

        self._ledge = [['X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'],
                       ['X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'],
                       ['X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X']]

        self.bossshape = [[' ', '_', '_', '_', '_', '_', '_', '_', ' '],
                          [' ', '-', '-', '-', '-', '-', '-', '-', ' '],
                          [' ', ' ', '@', '@', ' ', '@', '@', ' ', ' '],
                          [' ', ' ', '@', '@', ' ', '@', '@', ' ', ' '],
                          ['/', 'E', 'E', 'E', 'E', 'E', 'E', 'E', '\\'],
                          ['|', 'E', 'E', 'E', 'E', 'E', 'E', 'E', '|'],
                          ['|', 'E', 'E', 'E', 'E', 'E', 'E', 'E', '|'],
                          ['|', 'E', 'E', 'E', 'E', 'E', 'E', 'E', '|'],
                          [' ', '|', '|', '|', '|', '|', '|', '|', ' '],
                          [' ', '|', '|', '|', '|', '|', '|', '|', ' '],
                          [' ', '|', '|', '|', '|', '|', '|', '|', ' ']]

        self.enemyshape = [[' ', '@', '@', ' '], [
            '_', 'E', 'E', '_'], [' ', '|', '|', ' ']]

        self.marioshape = [['{', 'O', 'O', '}'], [
            '|', 'M', 'M', '|'], [' ', '/', '\\', ' ']]

        self._cloudpattern = [(5, 5), (3, 80), (3, 50),
                              (2, 160), (6, 190), (4, 240)]

        self._pipepattern = [(18, 260), (18, 80), (18, 350),
                             (18, 140), (18, 180), (18, 220)]

        self._ledgepattern = [(11, 270), (12, 380),
                              (13, 330), (12, 120), (8, 420)]

        self._enemypattern = [(24, 105), (24, 165),
                              (24, 245), (24, 295), (24, 375)]

        self._bossloc = (16, 425)

    def cloudsloc(self, number):
        '''returns the private variable'''

        return self._cloudpattern[number]

    def returncloud(self):
        '''returns the private variable'''

        return self._clouds

    def pipeloc(self, number):
        '''returns the private variable'''

        return self._pipepattern[number]

    def returnpipe(self):
        '''returns the private variable'''

        return self._pipe

    def ledgeloc(self, number):
        '''returns the private variable'''

        return self._ledgepattern[number]

    def returnledge(self):
        '''returns the private variable'''

        return self._ledge

    def enemyloc(self, number):
        '''returns the private variable'''

        return self._enemypattern[number]

    def bossloc(self):
        '''returns the private variable'''

        return self._bossloc
