"""Defining input class."""
import sys
import termios
import tty
import signal


class Get:
    """Class to get input."""

    def __call__(self):
        """Defining __call__."""
        filedesc = sys.stdin.fileno()
        old_settings = termios.tcgetattr(filedesc)
        try:
            tty.setraw(sys.stdin.fileno())
            charac = sys.stdin.read(1)
        finally:
            termios.tcsetattr(filedesc, termios.TCSADRAIN, old_settings)
        return charac


class AlarmException(Exception):
    """Handling alarm exception."""

    pass


def alarm_handler(signum, frame):
    """Handling timeouts."""

    raise AlarmException


def input_to(getch, timeout=0.1):
    """Taking input from user."""
    signal.signal(signal.SIGALRM, alarm_handler)
    signal.setitimer(signal.ITIMER_REAL, timeout)
    try:
        text = getch()
        signal.alarm(0)
        return text
    except AlarmException:
        signal.signal(signal.SIGALRM, signal.SIG_IGN)
        return None
