Welcome to Mario!
===================
*Coded by:*
**Sriven Reddy**

This **README** file contains :
 1.  Information About the Game
 2. Rules of the Game
 3. Instructions on how to Run the Code
 4. Requirements
 5. Implemented Features

----------


About The Game
-------------

The Mario (Japanese: マリオ Hepburn: Mario) franchise is a media franchise, published and produced by Japanese company Nintendo, starring the fictional Italian character Mario. It is primarily a video game franchise, with the franchise's other forms of media including several television series and a feature film. It was originally created by game designer Shigeru Miyamoto with the arcade game Donkey Kong, released on July 9, 1981. The games have been developed by a variety of developers including Nintendo, Hudson Soft, and AlphaDream. Most Mario games have either been released for the arcade or Nintendo video game consoles and handhelds dating from the Nintendo Entertainment System to the current generation of video game consoles. 
----------


Rules of the Game
-------------------

You control Mario, a little italian high on mushrooms, who is on a quest to kidnap bowsers wife. This version has only one level of the game and the goal is to reach the finish.
Enemies are red in coor and will kill you if you get close to them. You can jump over them to escape them or on them to kill them for extra points. All obstacles that mario has to jump over are in RED color. There is a boss at the end of the level who is faster and bigger and is MAGENTA in color. Killing the boss gives 500 extra points.
Reaching the flag pole at the end of the game completes the game and exits.
------------------------

How To Play:
------------------
>- IMPORTANT. PLEASE MAXIMISE YOUR TERMINAL WINDOW BEFORE YOU RUN THE GAME. THE GAME REQUIRES MINIMUM 160 COLUMNS AND 40 ROWS TO WORK PROPERLY
>- Run the following code to start the game.
```
python3 main.py
```
>- Press enter to start the game.
>- 'w, a, d' use these controls for jump, left, and right.
>- press 'q' to quit.

___________________

Reqiurements:
--------------------
- Python3
- Colorama library

_______________

Implemented Features:
--------------------
- Color
- Sounds

_______________

###Sriven Reddy
####20171081