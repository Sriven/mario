'''Mario class'''

import os
from person import Person


class Mario(Person):
    '''Mario class and method'''

    def __init__(self, x_in, y_in):

        super().__init__(x_in, y_in)
        self.jump = False

    def move(self, ch):
        '''Moving mario up and down'''

        if self.jump is False:
            if ch == 'w':
                os.system('aplay -q jump.wav&')
                self.jump = True
                self.vel = -15

    def check_jump(self, mat):
        '''Checking jump status'''

        if self.jump is True:

            if self.vel < 0:
                self.y_pos -= 1
                self.vel += 1

            elif self.vel >= 0:
                self.y_pos += 1
                self.vel += 1

            if self.y_pos <= 0:
                self.y_pos = 0

            if self.y_pos >= 26:
                self.y_pos = 26
                self.vel = 0
                self.jump = False

        self.collision_check(mat)

    def collision_check(self, mat):
        '''Checking vertical collisions'''

        if self.jump is True:

            if self.vel < 0:

                temp_x = mat[self.y_pos - 3][73]
                temp_y = mat[self.y_pos - 3][76]

                if temp_x == 'X' or temp_y == 'X':
                    self.vel = 0

            if self.vel >= 0:

                temp_x = mat[self.y_pos + 1][73]
                temp_y = mat[self.y_pos + 1][76]

                if temp_x == 'X' or temp_y == 'X':
                    self.jump = False
                    self.vel = 0

        elif self.jump is False:

            temp_x = mat[self.y_pos + 1][73]
            temp_y = mat[self.y_pos + 1][76]

            if temp_x != 'X' and temp_y != 'X':

                self.jump = True
                self.vel = 15
