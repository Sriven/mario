'''Base class'''
class Person():
    """Base"""
    def __init__(self, x_in, y_in):

        self.x_pos = x_in
        self.y_pos = y_in
        self.vel = 0
        self.dist = 0

    def move(self):
        '''Abstract Move function'''

        if self.dist >= 10:

            self.vel = -1

        if self.dist <= -10:

            self.vel = 1

        self.x_pos += self.vel
        self.dist += self.vel
