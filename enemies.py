'''why u do this'''
from person import Person


class Enemies(Person):
    '''Enemy Class'''

    def __init__(self, x_in, y_in):

        super().__init__(x_in, y_in)
        self.vel = 1

    def kill_me(self, mat, x_in, ran=4):
        '''Function to kill enemies'''
        for i in range(ran):

            if (self.x_pos - x_in) < 135 and (self.x_pos - x_in) > -1:

                temp1 = mat[self.y_pos - 1][self.x_pos - x_in + i]
                temp2 = mat[self.y_pos - 2][self.x_pos - x_in + i]

                if temp1 is not ' ' or temp2 is not ' ':
                    return True

        return False

    def die_mario(self, x_in, mario, ran=3):
        '''To kill Mario'''
        for i in range(1, ran):

            if (self.x_pos - x_in) < 145 and self.x_pos - x_in > -1:

                if self.y_pos + i == mario.y_pos and (self.x_pos - x_in - 4 == 73 or self.x_pos - x_in - 5 == 73):
                    return True

                if self.y_pos + i == mario.y_pos and (self.x_pos - x_in + 4 == 73 or self.x_pos - x_in + 5 == 73):
                    return True

        return False
