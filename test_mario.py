from board2 import Boardclass
from character import Mario
from map import Map
from enemies import Enemies
from boss import Boss
from input import Get, input_to

def test_mar():
	
	mario = Mario(75, 26)

	assert mario.x_pos == 75

def test_mar_jump():

	mario = Mario(75, 26)

	mario.move('w')

	assert mario.jump == True
	assert mario.vel == -15

def test_mar_jump2():

	MAP = Map()
	BOARD = Boardclass(32, 800, MAP)
	MARIO = Mario(75, 26)
	CH = Get()
	Y, X = MAP.bossloc()
	BOSSES = []
	BOSSES.append(Boss(X, Y))
	BOSS_KILLED = False
	ENEMIES = []
	SCORE = 0
	DEL_ENEMY = []

	for i in range(5):
	    Y, X = MAP.enemyloc(i)
	    ENEMIES.append(Enemies(X, Y))

	MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

	MARIO.move('w')

	while(MARIO.jump):

		temp1 = MARIO.vel
		temp2 = MARIO.y_pos

		MARIO.check_jump(MAT)

		if MARIO.jump is False:
			break

		assert MARIO.jump is True

		if temp1 < 0:
			assert temp1 == MARIO.vel - 1
			assert temp2 == MARIO.y_pos + 1

		elif temp1 >= 0:
			assert temp1 == MARIO.vel - 1
			assert temp2 == MARIO.y_pos - 1


def test_mar_col():

	MAP = Map()
	BOARD = Boardclass(32, 800, MAP)
	MARIO = Mario(75, 26)
	CH = Get()
	Y, X = MAP.bossloc()
	BOSSES = []
	BOSSES.append(Boss(X, Y))
	BOSS_KILLED = False
	ENEMIES = []
	SCORE = 0
	DEL_ENEMY = []

	for i in range(5):
	    Y, X = MAP.enemyloc(i)
	    ENEMIES.append(Enemies(X, Y))

	MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

	BOARD.move_screen('a', MARIO)

	assert BOARD.x_pos == 0

	BOARD.move_screen('d', MARIO)

	assert BOARD.x_pos == 2

	for i in range(10):
		MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)
		BOARD.move_screen('d', MARIO)

	assert BOARD.x_pos == 6

def test_mar_col2():

	MAP = Map()
	BOARD = Boardclass(32, 800, MAP)
	MARIO = Mario(75, 26)
	CH = Get()
	Y, X = MAP.bossloc()
	BOSSES = []
	BOSSES.append(Boss(X, Y))
	BOSS_KILLED = False
	ENEMIES = []
	SCORE = 0
	DEL_ENEMY = []

	for i in range(5):
	    Y, X = MAP.enemyloc(i)
	    ENEMIES.append(Enemies(X, Y))

	for i in range(10):
		MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)
		BOARD.move_screen('d', MARIO)

	assert BOARD.x_pos == 6

def test_mar_jump_move():

	MAP = Map()
	BOARD = Boardclass(32, 800, MAP)
	MARIO = Mario(75, 26)
	CH = Get()
	Y, X = MAP.bossloc()
	BOSSES = []
	BOSSES.append(Boss(X, Y))
	BOSS_KILLED = False
	ENEMIES = []
	SCORE = 0
	DEL_ENEMY = []

	for i in range(5):
	    Y, X = MAP.enemyloc(i)
	    ENEMIES.append(Enemies(X, Y))

	MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

	MARIO.move('w')

	while(MARIO.jump):

		temp3 = BOARD.x_pos
		temp1 = MARIO.vel
		temp2 = MARIO.y_pos

		BOARD.move_screen('a', MARIO)

		if temp3 != 0:
			assert BOARD.x_pos == temp3 - 2

		temp3 = BOARD.x_pos

		BOARD.move_screen('d', MARIO)

		if temp3 != 1448:
			assert BOARD.x_pos == temp3 + 2

		MARIO.check_jump(MAT)

		if MARIO.jump is False:
			break

		assert MARIO.jump is True

		if temp1 < 0:
			assert temp1 == MARIO.vel - 1
			assert temp2 == MARIO.y_pos + 1

		elif temp1 >= 0:
			assert temp1 == MARIO.vel - 1
			assert temp2 == MARIO.y_pos - 1

def test_mar_jump_move2():

	MAP = Map()
	BOARD = Boardclass(32, 800, MAP)
	MARIO = Mario(75, 26)
	CH = Get()
	Y, X = MAP.bossloc()
	BOSSES = []
	BOSSES.append(Boss(X, Y))
	BOSS_KILLED = False
	ENEMIES = []
	SCORE = 0
	DEL_ENEMY = []

	for i in range(5):
	    Y, X = MAP.enemyloc(i)
	    ENEMIES.append(Enemies(X, Y))

	MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

	MARIO.move('w')

	while(MARIO.jump):

		MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

		temp3 = BOARD.x_pos
		temp1 = MARIO.vel
		temp2 = MARIO.y_pos

		BOARD.move_screen('d', MARIO)

		if BOARD.check_horizontal_collision('d', MARIO) is False:
			assert BOARD.x_pos == temp3 + 2

		MARIO.check_jump(MAT)

		if MARIO.jump is False:
			break

		assert MARIO.jump is True

		if temp1 < 0 and MARIO.vel != 0:
			assert temp1 == MARIO.vel - 1
			assert temp2 == MARIO.y_pos + 1

		elif temp1 >= 0:
			assert temp1 == MARIO.vel - 1
			assert temp2 == MARIO.y_pos - 1


def test_mar_enemykill():

	MAP = Map()
	BOARD = Boardclass(32, 800, MAP)
	MARIO = Mario(75, 26)
	CH = Get()
	Y, X = MAP.bossloc()
	BOSSES = []
	BOSSES.append(Boss(X, Y))
	BOSS_KILLED = False
	ENEMIES = []
	SCORE = 0
	DEL_ENEMY = []

	for i in range(5):
	    Y, X = MAP.enemyloc(i)
	    ENEMIES.append(Enemies(X, Y))

	MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

	while(ENEMIES[0].die_mario(36, MARIO) is False):

		MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

		ENEMIES[0].move()

		assert ENEMIES[0].die_mario(36, MARIO) is False

# def test_mar_enemydie():

# 	MAP = Map()
# 	BOARD = Boardclass(32, 800, MAP)
# 	MARIO = Mario(75, 22)
# 	CH = Get()
# 	Y, X = MAP.bossloc()
# 	BOSSES = []
# 	BOSSES.append(Boss(X, Y))
# 	BOSS_KILLED = False
# 	ENEMIES = []
# 	SCORE = 0
# 	DEL_ENEMY = []

# 	for i in range(5):
# 	    Y, X = MAP.enemyloc(i)
# 	    ENEMIES.append(Enemies(X, Y))

# 	MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

# 	while(ENEMIES[0]):

# 		MAT = BOARD.print_screen(MARIO, MAP, ENEMIES, BOSSES)

# 		ENEMIES[0].move()
# 		ENEMIES[0].kill_me(MAT, 30)










